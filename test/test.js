//const assert = require('assert');
var assert = require('chai').assert


// init warehouse class
const lib_warehouse = require('../src/lib/warehouse_module_class')
let app = {
    warehouse: lib_warehouse
}


console.log("Database application just started, so forklifts have no status associated")

describe('warehouse_module_class', function() {
    describe('_setForkliftStatus', function() {
        describe('=> arguments check', function() {
            it('should throw error when no parameters are passed', function() {
                function setForkliftParmsErr() {
                    app.warehouse._setForkliftStatus();
                }
                assert.throws(setForkliftParmsErr, Error, "Parameters error: expected 2 arguments")
            })

            it('should throw error when too many parameters are passed (3 in this test)', function() {
                function setForkliftParmsErr() {
                    app.warehouse._setForkliftStatus(1, "ciao", {});
                }
                assert.throws(setForkliftParmsErr, Error, "Parameters error: expected 2 arguments")
            })

            let tests = {
                "ciao": "string",
                1.2: "float",
                undefined: "undefined"
            }
            for ( t in tests ) {
                it('should throw error: first parameter type error (passing ' + tests[t] + ')', function() {
                    function setForkliftParmsErr() {
                        app.warehouse._setForkliftStatus(t, "ciao");
                    }
                    assert.throws(setForkliftParmsErr, TypeError, "Parameter type error: integer expected")
                })
            }

            it('should throw error: second parameter type error (passing integer)', function() {
                function setForkliftParmsErr() {
                    app.warehouse._setForkliftStatus(1, 1);
                }
                assert.throws(setForkliftParmsErr, TypeError, "Parameter type error: string expected")
            })

            it('should throw error: second parameter type error (passing undefined)', function() {
                function setForkliftParmsErr() {
                    app.warehouse._setForkliftStatus(1, undefined);
                }
                assert.throws(setForkliftParmsErr, TypeError, "Parameter type error: string expected")
            })

            it('should throw error: second parameter not exact', function() {
                function setForkliftParmsErr() {
                    app.warehouse._setForkliftStatus(1, "ciao");
                }
                assert.throws(setForkliftParmsErr, Error, "Parameter type error: expect one of this.ACTIONS constants")
            })
        })
    })
})

describe('warehouse_module_class', function() {
    describe('_setForkliftStatus', function() {
        it('should accept new status', function() {
            app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.FORKLIFT_SWITCHED_ON)
        })
    })
})



//assert.equal([1, 2, 3].indexOf(4), -1);