#!/bin/bash

DB_FILE="./data/warehouse.sqlite"
# Create data folder
mkdir -p data

# create table OBJECTS
CREATE_TABLE_OBJECTS="CREATE TABLE IF NOT EXISTS objects (\n"
CREATE_TABLE_OBJECTS+="   code                VARCHAR PRIMARY KEY,                -- barcode\n"
CREATE_TABLE_OBJECTS+="   code_type           VARCHAR,                            -- code128, code39, QRCode, ...\n"
CREATE_TABLE_OBJECTS+="   status              VARCHAR,                            -- static, moving, processing, processed\n"
CREATE_TABLE_OBJECTS+="   length_in_cm        NUMBER,\n"
CREATE_TABLE_OBJECTS+="   width_in_cm         NUMBER,\n"
CREATE_TABLE_OBJECTS+="   height_in_cm        NUMBER,\n"
CREATE_TABLE_OBJECTS+="   weight_in_hkg       NUMBER,\n"
CREATE_TABLE_OBJECTS+="   material_type       VARCHAR,\n"
CREATE_TABLE_OBJECTS+="   material_subtype    VARCHAR,\n"
CREATE_TABLE_OBJECTS+="   position_x          REAL,\n"
CREATE_TABLE_OBJECTS+="   position_y          REAL,\n"
CREATE_TABLE_OBJECTS+="   position_z          REAL,\n"
CREATE_TABLE_OBJECTS+="   last_action_hr      VARCHAR,\n"
CREATE_TABLE_OBJECTS+="   last_action_ts      VARCHAR,\n"
CREATE_TABLE_OBJECTS+="   creation_dt         VARCHAR DEFAULT CURRENT_TIMESTAMP\n"
CREATE_TABLE_OBJECTS+=");"
echo -e "$CREATE_TABLE_OBJECTS"|sqlite3 $DB_FILE

# create table OBJECTS_POSITIONS_HISTORY
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY="CREATE TABLE IF NOT EXISTS objects_positions_history (\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   object_code     VARCHAR,                                -- extern key\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   movement_hr     VARCHAR,                                -- date time human readable\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   movement_ts     VARCHAR,                                -- timestamp\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   action          VARCHAR,                                -- what user did with the object\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   forklift_id     INT,\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   position_x      REAL,\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   position_y      REAL,\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+="   position_z      REAL\n"
CREATE_TABLE_OBJECTS_POSITIONS_HISTORY+=");"

echo -e "$CREATE_TABLE_OBJECTS_POSITIONS_HISTORY"|sqlite3 $DB_FILE