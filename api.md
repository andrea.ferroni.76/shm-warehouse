Implemented API
===============

[TOC]

List by actions
---------------


### Set forklift status to switched on/off

Set forklift to specified status

**method**: PUT
**path**: /forklift/:id/setStatus/:status
**since**: 0.4.0

**Parameters**:
  * `id`: integer number greater than one representing forklist id
  * `status`: one of "switchon"|"switchoff" values

#### Details

The request calls `_setForkliftStatus`.
This method calls `_checkForkliftActionCoherence` to warn (in a log) about eventually _strange_ status sequence changes.
Then the new status is assigned and logged.

Calls workflow:
  * check new forklift status against older one: if not coherent, log it
  * set the new status as `switched on`
  * log the new forklift status

Calls:
  * `wareHouseClass.forkliftSwitchedOn(id)`
    * -> `_setForkliftStatus(id, ACTIONS.FORKLIFT_SWITCHED_ON)`
      * -> `_checkForkliftActionCoherence(status)` with status=`ACTIONS.FORKLIFT_SWITCHED_ON`
      * -> `_logAction(text)` where text desccribes new assignment


### Set object as moving

Set specified object to `moving` status

**method**: PUT
**path**: /forklift/:id/moveObject/:code/position/:x/:y/:z
**since**: 0.4.2

#### Details

Many other operations and checks are executed

Calls workflow;
  * identify if passed barcode is known, new or even not passed:
    * if is **unknown**:
    * if is **new**:
    * if is **known**:
      * check new forklift status against older one: if not coherent, log it
      * try to update status and position
        * if the object is STATIC or PROCESSING:
          * position and status are update
          * history entry is created
          * warehouse data are persisted

Calls:
  * `wareHouseClass.startMovingObject(flid, pos, barcode)`
    * `_identifyObjectTypeByBarcode(barcode)`:
      * is **unknown**:
      * is **new**:
      * is **kown**:
        * -> `_setForkliftStatus(id, ACTIONS.MOVING_AN_OBJECT)`
          * -> `_checkForkliftActionCoherence(status)` with status=`ACTIONS.MOVING_AN_OBJECT`
        * -> `object.updatePositionAndStatus(position, warehouseObject.STATUS.MOVING)`
          * according to required status check if new one is allowed; if so, update status and position and log position _error_
        * if object is updated, update warehouse data and `this._addHistoryEntry(barcode, position, forkliftId, this.ACTIONS.MOVING_AN_OBJECT)`


### Set object as static

Set object to `static` status

**method**: PUT
**path**: /forklift/:id/depositObject/:code/position/:x/:y/:z
**since**: 0.4.2

#### Details

...add...

---

List by calls, grouped by methods
---------------------------------

### PUT

#### /forklift/:id/setStatus/:status

[vedi dettagli](#set-forklift-status-to-switched-on)