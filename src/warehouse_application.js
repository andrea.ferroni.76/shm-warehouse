/**
 * @name:       Warehouse
 *              Part of Storehouse Manager Suite
 * @abstract:   Storage application driven by API REST
 * @author:     Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * @file        warehouse_application.js
 *
 * @todo
 * @todo
 *
 * Repository:  <git@gitlab.com:andrea.ferroni.76/shm-warehouse.git>
 */

'use strict'



//
// [ Load dependencies ]
//

// configuration and constants
const CONF = require('./conf.js')                               // application configuration
// application libraries
const lib_warehouse = require('./lib/warehouse_module_class')
// shared librearies
const lib_math = require("mathLibrary")
// external (NPM) libraries
const express = require('express')                              // "Framework web veloce, non categorico e minimalista per Node.js"
const httpStatus = require('http-status-codes')                 // HTTP status codes constants
const _isUndefined = require('lodash/isUndefined')              // lodash.isUndefined


//
// end [ Load dependencies ]


//
// [ Application object ]
//

let app = {
    // properties
    env: process.env.NODE_ENV,                                  // environment passed variables ("test"|"staging"|"production")
    conf: {
        appConf: CONF,                                          // all configuration variables and data in conf.js file
        whatToLog: {
            applicationActivity: true,                          // application core activity: DO NOT DISABLE
            APIActivity: true                                   // requests activity: DO NOT DISABLE
        }
    },
    webServer: express(),                                       // express HTTP server
    warehouse: lib_warehouse
}

//
// end [ Application object ]



app.warehouse.init(app.conf.appConf)



//
// [ Initialize and set API REST ]
//

app.webServer.get('/', function (req, res) {
    res.send('Hello World!');
});

// Since 0.4.0
// set forklift status to "switched on" or "switched off"
app.webServer.put('/forklift/:id/setStatus/:status', function (req, res) {

    let badRequest = false

    let statuses = {
        switchOn: "switchon",
        switchOff: "switchoff"
    }
    Object.freeze(statuses)

    // check request data
    if ( typeof req.params.id == app.warehouse.UNDEFINED ) badRequest = true                // parameter id must exist...
    if ( !lib_math.isIntegerString(req.params.id) ) badRequest = true                       // ...and be an integer
    if ( typeof req.params.status != "string" ) badRequest = true                           // parameter status must exist...
    if ( Object.values(statuses).indexOf(req.params.status) == -1 ) badRequest = true       // ...and be "switchon"|"switchoff"

    if ( badRequest ) {
        res.status(httpStatus.BAD_REQUEST).send('not valid request parameters')
        return
    }

    let forkliftId = Number.parseInt(req.params.id)
    let requestedStatus = req.params.status

    switch (requestedStatus) {
        case statuses.switchOn:
            app.warehouse.forkliftSwitchedOn(forkliftId)
            break
        case statuses.switchOff:
            app.warehouse.forkliftSwitchedOff(forkliftId)
            break
    }

    res.status(httpStatus.OK).send()
});

// Since 0.4.2
// a specified forklift picks-up an object given the code or "unknown" from a given position
app.webServer.put('/forklift/:id/moveObject/:code/position/:x/:y/:z', function(req, res) {

    let badRequest = false

    // check request data
    if ( typeof req.params.id == app.warehouse.UNDEFINED ) badRequest = true                // parameter id must exist...
    if ( !lib_math.isIntegerString(req.params.id) ) badRequest = true                       // ...and be an integer
    if ( _isUndefined(req.params.code) ) badRequest = true                                  // parameter code must exist (content is not checked)
    if ( !lib_math.isFloatString(req.params.x)) badRequest = true                           // x, y and z must be float strings
    if ( !lib_math.isFloatString(req.params.y)) badRequest = true
    if ( !lib_math.isFloatString(req.params.z)) badRequest = true

    if ( badRequest ) {
        res.status(httpStatus.BAD_REQUEST).send('not valid request parameters')
        return
    }

    let forkliftId = Number.parseInt(req.params.id)
    let barCode = req.params.code != "unknown" ? req.params.code : undefined
    let position = {
        x: Number.parseFloat(req.params.x),
        y: Number.parseFloat(req.params.y),
        z: Number.parseFloat(req.params.z),
    }

    let opRes = app.warehouse.startMovingObject(forkliftId, position, barCode)

    if (opRes) {
        res.status(httpStatus.OK).send()}
    else {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            executed: false,
            message: app.warehouse.lastErrorMessage
        })
    }
})

// Since 0.4.2
// a specified forklift deposits an object given the code or "unknown" to a given position
app.webServer.put('/forklift/:id/depositObject/:code/position/:x/:y/:z', function(req, res) {

    let badRequest = false

    // check request data
    if ( typeof req.params.id == app.warehouse.UNDEFINED ) badRequest = true                // parameter id must exist...
    if ( !lib_math.isIntegerString(req.params.id) ) badRequest = true                       // ...and be an integer
    if ( _isUndefined(req.params.code) ) badRequest = true                                  // parameter code must exist (content is not checked)
    if ( !lib_math.isFloatString(req.params.x)) badRequest = true                           // x, y and z must be float strings
    if ( !lib_math.isFloatString(req.params.y)) badRequest = true
    if ( !lib_math.isFloatString(req.params.z)) badRequest = true

    if ( badRequest ) {
        res.status(httpStatus.BAD_REQUEST).send('not valid request parameters')
        return
    }

    let forkliftId = Number.parseInt(req.params.id)
    let barCode = req.params.code != "unknown" ? req.params.code : undefined
    let position = {
        x: Number.parseFloat(req.params.x),
        y: Number.parseFloat(req.params.y),
        z: Number.parseFloat(req.params.z),
    }

    let opRes = app.warehouse.endMovingObject(forkliftId, position, barCode)

    if (opRes) {
        res.status(httpStatus.OK).send()}
    else {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            executed: false,
            message: app.warehouse.lastErrorMessage
        })
    }
})



app.webServer.listen(app.conf.appConf.webServer.port, function () {
    console.log(`Warehouse app listening on port ${app.conf.appConf.webServer.port}`);
});

//
// end [ Initialize and set API REST ]


// SOME BASIC TESTS
/*app.warehouse.init({fileData: "./db/warehouse.sqlite"})
app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.FORKLIFT_SWITCHED_ON)
app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.FORKLIFT_SWITCHED_ON)

app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.MOVING_AN_OBJECT)
app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.MOVING_AN_OBJECT)

app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.RELEASING_AN_OBJECT)
app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.RELEASING_AN_OBJECT)

app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.FORKLIFT_SWITCHED_OFF)
app.warehouse._setForkliftStatus(1, app.warehouse.ACTIONS.FORKLIFT_SWITCHED_OFF)


// TEST 1
// Fork 2:
//  - picks up a new object (barcode is clearly read)
//  - releases same object
let forkID = 2
let barcode = "ABCD-1234"
app.warehouse.startMovingObject(forkID, {x: 12, y:10, z: 0}, barcode)               // add new item and set as MOVING
app.warehouse.startMovingObject(forkID, {x: 14, y:21, z: 0}, barcode)               // should not work because ABCD-1234 is MOVING
app.warehouse.endMovingObject(forkID, {x: 14, y:21, z: 0}, barcode)                 // the object with barcode ABCD-1234 is STATIC

// TEST 2
// Fork 1:
//  - picks up a new object (barcode is clearly read)
//  - releases the same object
forkID = 1
barcode = "AABB-0011"
app.warehouse.startMovingObject(forkID, {x: 12.5, y:10, z: 0}, barcode)             // add new item and set as MOVING
app.warehouse.endMovingObject(forkID, {x: 13.30, y:21.45, z: 1.47}, barcode)        // the object with barcode AABB-0011 is STATIC

// TEST 3
// Fork 1
//  - moves an object in the warehouse
forkID = 1
barcode = "ABCD-1234"
app.warehouse.startMovingObject(forkID, {x: 12, y:10, z: 0}, barcode)               // set item as MOVING
app.warehouse.endMovingObject(forkID, {x: 14, y:21, z: 3}, barcode)                 // the object with barcode AABB-0011 is STATIC

// TEST 4
// Fork 1
//  - moves an object with an unread barcode
forkID = 1
barcode = undefined
app.warehouse.startMovingObject(forkID, {x: 12, y:6, z: 0}, barcode)                // set item as MOVING
app.warehouse.startMovingObject(forkID, {x: 14, y:21, z: 3}, barcode)               // should warn that this forklift loses a movementation
app.warehouse.endMovingObject(forkID, {x: 14, y:21, z: 3}, barcode)                 // the object with barcode AABB-0011 is STATIC
*/

//
// Apply ENV settings
//

let allowedEnvs = {
    development: "development",
    test: "test",
    production: "production"
}
Object.freeze(allowedEnvs)

switch (process.env.NODE_ENV) {
    case allowedEnvs.development:
        app.warehouse.conf.enableConsoleLog = true                      // log to console
        app.warehouse.conf.warehouseByCodesHistoryInMemory = true       // keep objects history in memory
        break
    case allowedEnvs.test:
        app.warehouse.conf.enableConsoleLog = true                      // log to console
        app.warehouse.conf.warehouseByCodesHistoryInMemory = true       // keep objects history in memory
        break
    case allowedEnvs.production:
        app.warehouse.conf.enableConsoleLog = false                     // DO NOT log to console
        app.warehouse.conf.warehouseByCodesHistoryInMemory = false      // DO NOT keep objects history in memory
        break
}


// CALLS FOR TEST
//
// TEST 1
// ------
// Forklist 1 takes the object with code 12340123 from position coordinates [1, 2, 3]
// PUT - http://192.168.1.216:1050/forklift/1/moveObject/12340123/position/1/2/3
//
// and then deposits it at position [4, 5, 6]
// PUT - http://192.168.1.216:1050/forklift/1/depositObject/12340123/position/4/5/6
//
// Atteso: il software salva nel database i dati dell'oggetto ad ogni movimento
//         e 3 eventi nell'history relativa: creazione oggetto, spostamento e deposito

