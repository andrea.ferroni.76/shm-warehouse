"use strict"

/**
 * @name:       warehouse_module_class
 * @abstract    warehouse class
 * @author      Andrea Ferroni <andrea.ferroni.76@gmail.com>
 * @file        warehouse_module_class.js
 * @version     0.5.0
 *
 * @todo _askObjectData: remove random type code (see REMOVE in comments)
 * @todo __REMOVEME__generateFakeObjectData: remove
 * @todo startMovingObject: add checks if barcode is unknown (no more than one unknown object for forklift id)
 * @todo startMovingObject: in case 3 add the movementation to history
 *
 * @repository  <git@gitlab.com:andrea.ferroni.76/shm-warehouse.git>
 */


const lib_shmMathLib = require('mathLibrary');                  // math module library
const _isFunction = require('lodash/isFunction')                // lodash.isFunction
const _difference = require('lodash/difference')                // lodash.difference
const Freezer = require('freezer-js');                          // Freezer external library
const sqlite3 = require('sqlite3').verbose();


/**
 * Manage data for a warehouse
 * @class wareHouseClass
 * @access public
 */
class wareHouseClass {

    constructor() {

        this.errorMessage = ''

        let closeThis = this

        /**
         * Current forklifts status
         * @type {string[]}
         */
        this.forkLiftsWork = []

        /**
         * warehouse database
         * @type {object} warehouse Freezer object
         */
        this.warehouseByCodes = new Freezer({
            lastObjectCode: undefined
        })

        /**
         * Warehouse objects history data
         * Is not populated if {@link wareHouseClass#conf.warehouseByCodesHistoryInMemory} is set false
         * @type {object}
         */
        this.warehouseByCodesHistory = {}

        /**
         * @type {object[]} - each forklift has one movementation of unknown object
         */
        this.unkownMovementationsByForkLift = []
        /**
         * @type {function} - function to call as external desired logger: a strings list is passed
         */
        this.externalLogger = undefined

        this.UNDEFINED = "undefined"
        Object.freeze(this.UNDEFINED)

        this.DATA_PERSISTENCE = {
            IMMEDIATE: "immediate",
            DELAYED: "delayed"
        }
        Object.freeze(this.DATA_PERSISTENCE)

        this.ACTIONS = {
            FORKLIFT_SWITCHED_ON: "switching forklift on",
            FORKLIFT_SWITCHED_OFF: "switching forklift off",
            JUST_CREATED: "just created",
            MOVING_A_NEW_OBJECT: "moving a new object",
            MOVING_AN_OBJECT: "moving an object",
            MOVING_AN_UNKNOWN_OBJECT: "moving an unknown object",
            RELEASING_A_NEW_OBJECT: "releasing a new object",
            RELEASING_AN_OBJECT: "releasing an object",
            RELEASING_AN_UNKNOWN_OBJECT: "releasing an unknown object",
        }
        Object.freeze(this.ACTIONS)

        this.OBJECT_STATUSES = {
            STATIC: "static",
            MOVING: "moving",
            PROCESSING: "processing",
            PROCESED: "processed"
        }
        Object.freeze(this.OBJECT_STATUSES)

        // available object types
        this.OBJECT_TYPES = {
            NEW: "new object",              // when barcode passed matches no one in the warehouse
            KNOWN: "known object",           // when barcode passed is a new one
            UNKNOWN: "unknown object"       // when no barcode is passed
        }
        Object.freeze(this.OBJECT_TYPES)

        this.SUPPORTED_STORAGE_SYSTEM = {
            SQLite: "SQLite"
        }
        Object.freeze(this.SUPPORTED_STORAGE_SYSTEM)

        /**
         * Class behaviuor configuration
         * @typedef {Object} configurationObject
         * @property {boolean} warehouseByCodesHistoryInMemory - Indicates whether to store objects history in memory or not
         * @property {boolean} enableConsoleLog - Indicates whether log to console or not (set false in production)
         * @property {object} dateTime - date and time settings
         */
        /**
         * Class behaviour configuration
         * @type {configurationObject}
         */
        this.conf = {
            dataPresistence: this.DATA_PERSISTENCE.IMMEDIATE,
            warehouseByCodesHistoryInMemory: true,                          // set false in production env
            enableConsoleLog: true,                                         // set false in production env
            store: {
                enabled: ["SQLite"],
                SQLite: {
                    persistence: this.DATA_PERSISTENCE.IMMEDIATE,           // persist data immediately
                    filename: undefined,                                    // db file name (set in init)
                    path: undefined                                         // db file path (set in init)
                },
                Redis: {                                                    // actually not taken in account
                    persistence: this.DATA_PERSISTENCE.DELAYED              // persist data when necessary
                }
            },
            dateTime: {
                humanReadableFormat: {
                    timeZone: 'Europe/Rome',
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit',
                    second: '2-digit'
                }
            }
        }
    }


    init(conf) {
        // SQLite storage initialization if configuration has SQLite database settings
        if ( typeof conf.database.SQLite != this.UNDEFINED ) {

            let closeThis = this

            let filename = conf.database.SQLite.filename
            let filepath = conf.database.SQLite.filepath
            this.conf.store.SQLite.path = filepath
            this.conf.store.SQLite.filename = filename

            var warehouseObjectsData = {}

            // if implementation supports SQLite
            if (this.conf.store.enabled.indexOf(this.SUPPORTED_STORAGE_SYSTEM.SQLite) != -1) {

                this.STORE_SQLite = new wareHouseData_SQLite_Class(filename, filepath)      // set variables class
                this.STORE_SQLite.loadOjectsData(function(e,d){
                    d.forEach(function(el){
                        // create new warehouse object
                        warehouseObjectsData[el.code] = new warehouseObject(el.code)
                        warehouseObjectsData[el.code].setDataFromSQLRecordToObject(el)
                    })
                    // finalize
                    warehouseObjectsData["lastObjectCode"] = "loaded"
                    closeThis.warehouseByCodes = new Freezer(warehouseObjectsData)              // creating new Freezer object

                    let closeThat = closeThis
                    closeThis.warehouseByCodes.on('update', function(curState, oldState){
                        let oldCodesLen = Object.keys(oldState).length
                        let newCodesLen = Object.keys(curState).length
                        let operation = "update"
                        if (oldCodesLen > newCodesLen) {
                            operation = "remove"
                        }
                        if (oldCodesLen < newCodesLen) {
                            operation = "add"
                        }
                        if ( curState.lastObjectCode == "loaded") {
                            console.log("warehouse loaded")
                            return
                        }

                        console.log(`warehouse: ${operation} object with code ${curState.lastObjectCode}`)
                        closeThat._storeData("object", operation, curState[curState.lastObjectCode])
                    })

                    console.log(`Warehouse data loaded with ${d.length} objects`)
                })

            }

        }
    }


    //
    // GETTERS AND SETTERS
    //

    get lastErrorMessage() {
        return this.errorMessage
    }

    /**
     * Get italian formatted date and time. Format is specified in {@link #configurationobject}
     *
     * @type {string} The italian formatted date and time
     * @private
     * @memberof wareHouseClass
     */
    get _readableItalianDateTime() {
        let fmt = this.conf.dateTime.humanReadableFormat
        let today = new Date()
        return today.toLocaleString("it-IT", fmt)
    }

    /**
     * Convert passed timestamp to ilalian formatted date and time
     *
     * @param {number} ts - is a positive timestamp integer in Javascript stype (milliseconds)
     * @returns {string} formatted date and time
     */
    _readableItalianDateTimeFromTimestamp(ts) {
        if (!Number.isInteger(ts)) return ''                // parameter must be a number
        let fmt = this.conf.dateTime.humanReadableFormat
        let dateToConvert = new Date(ts)
        return dateToConvert.toLocaleString("it-IT", fmt)
    }

    //
    // PUBLIC METHODS
    //

    /**
     * Assign SWITCHED_ON status to specified forklift
     *
     * @param {number} [forkliftId=0] forklift id number (must be positive)
     * @returns {boolean} true if successful, false if not
     * @memberof wareHouseClass
     */
    forkliftSwitchedOn(forkliftId = 0) {
        if (!Number.isInteger(forkliftId) || forkliftId < 1) return false
        this._setForkliftStatus(forkliftId, this.ACTIONS.FORKLIFT_SWITCHED_ON)
        return true
    }

    /**
     * Assign status SWITCHED_OFF to passed id forklift
     *
     * @param {number} [forkliftId=0] forklift identifier number
     * @returns {boolean} true if successful, false if not
     * @memberof wareHouseClass
     */
    forkliftSwitchedOff(forkliftId = 0) {
        if (!Number.isInteger(forkliftId) || forkliftId < 1) return false
        this._setForkliftStatus(forkliftId, this.ACTIONS.FORKLIFT_SWITCHED_OFF)
        return false
    }


    /**
     * Set an object as moving. Barcode can exist in the warehouse, not exist or even not be passed
     *
     * @param {number} [forkliftId=0]  forklift identifier number
     * @param {object} [position={x: 0, y: 0, z: 0}] object position in space (object must contain x, y and z numeric fields)
     * @param {string} [barcode=this.UNDEFINED] barcode identifier
     * @returns {boolean}
     * @memberof wareHouseClass
     */
    startMovingObject(forkliftId = 0, position = {x: 0, y: 0, z: 0}, barcode = undefined) {

        if (!Number.isInteger(forkliftId) || forkliftId < 1) return false       // check barcode parameter type

        // recognize object type by barcode (is a new one? or... ?)
        let objectType = this._identifyObjectTypeByBarcode(barcode),
            objectIsUpdated

        // do things according barcode presence
        switch (objectType) {

            // 1. moving object without barcode
            case this.OBJECT_TYPES.UNKNOWN:

                // set forklift status
                this._setForkliftStatus(forkliftId, this.ACTIONS.MOVING_AN_UNKNOWN_OBJECT)

                let now = + new Date()
                let nowHumanReadable = this._readableItalianDateTime
                // create temporary movementation object data
                let obj = {
                    forkID: forkliftId,                                     // set folklift id
                    actionDateTimeStart_ts: now,
                    actionDateTimeStart_hr: nowHumanReadable,
                    actionDateTimeEnd_ts: undefined,
                    actionDateTimeEnd_hr: undefined,
                    positionFrom: {                                         // set the position
                        x: position.x,
                        y: position.y,
                        z: position.z
                    },
                    positionTo: {                                           // set the position
                        x: undefined,
                        y: undefined,
                        z: undefined
                    }
                }

                // verify if forklift loses a previous unknown object movementation
                // if the same fork
                if (this.unkownMovementationsByForkLift[forkliftId] != undefined) {
                    // date time start
                    let dts = this.unkownMovementationsByForkLift[forkliftId].actionDateTimeStart_hr
                    // date time end
                    let dte = this.unkownMovementationsByForkLift[forkliftId].actionDateTimeEnd_hr
                    if (dte == undefined) {
                        dte = "(still moving)"
                    }
                    console.warn(`Warning: forklift ${forkliftId} loses a previous unknown object movementation at ${dts} - ${dte}`)
                }

                // add object to unknown movementations forklift associated list
                this.unkownMovementationsByForkLift[forkliftId] = obj

                break

            // 2. moving an object with a new barcode (not present in the warehouse)
            case this.OBJECT_TYPES.NEW:

                // set forklift status
                this._setForkliftStatus(forkliftId, this.ACTIONS.MOVING_A_NEW_OBJECT)

                // create a new product
                let newObject = new warehouseObject(barcode)
                newObject.setPositionAndStatus(position, this.OBJECT_STATUSES.MOVING)
                let objectData = this._askObjectData()
                newObject.setPhysicalProperties({
                    length: objectData.length,
                    width: objectData.width,
                    height: objectData.height
                },
                objectData.weight, {
                    type: objectData.materialType,
                    subType: objectData.materialSubType
                })
                // add created object to warehouse
                this.warehouseByCodes.get().set(barcode, newObject).set('lastObjectCode', barcode)
                // add to object history the creation event
                this._addHistoryEntry(barcode, position, forkliftId, this.ACTIONS.JUST_CREATED)
                // add to object history the movementation event
                this._addHistoryEntry(barcode, position, forkliftId, this.ACTIONS.MOVING_A_NEW_OBJECT)

                // set current forklift status
                this.forkLiftsWork[forkliftId] = this.ACTIONS.MOVING_A_NEW_OBJECT

                objectIsUpdated = true

                break

            // 3. moving a known object
            case this.OBJECT_TYPES.KNOWN:

                // set forklift status
                this._setForkliftStatus(forkliftId, this.ACTIONS.MOVING_AN_OBJECT)

                // try to set new object status
                let object = this.warehouseByCodes.get()[barcode]
                objectIsUpdated = object.updatePositionAndStatus(position, warehouseObject.STATUS.MOVING)

                // if object is correctly updated
                if (objectIsUpdated) {
                    // update Freezer data (this triggers object data save)
                    this.warehouseByCodes.get().set(barcode, object)
                    this.warehouseByCodes.get().set('lastObjectCode', barcode)
                    // add to object history the new position and action
                    this._addHistoryEntry(barcode, position, forkliftId, this.ACTIONS.MOVING_AN_OBJECT)
                }

                break
        }

        return objectIsUpdated
    }

    endMovingObject(forkliftId, position, barcode = undefined) {

        // identify movementation situation
        let objectType = this._identifyObjectTypeByBarcode(barcode),
            objectIsUpdated

        // do things according barcode presence
        switch (objectType) {

            // 1. end moving object without barcode
            case this.OBJECT_TYPES.UNKNOWN:

                // set current forklift status
                this._setForkliftStatus(forkliftId, this.ACTIONS.RELEASING_AN_UNKNOWN_OBJECT)

                // set completition date time and position
                let now = + new Date()
                let nowHumanReadable = this._readableItalianDateTime
                this.unkownMovementationsByForkLift[forkliftId].actionDateTimeEnd_ts = now
                this.unkownMovementationsByForkLift[forkliftId].actionDateTimeEnd_hr = nowHumanReadable
                this.unkownMovementationsByForkLift[forkliftId].positionTo.x = position.x
                this.unkownMovementationsByForkLift[forkliftId].positionTo.y = position.y
                this.unkownMovementationsByForkLift[forkliftId].positionTo.z = position.z

                break

            // 2. end moving an object with a new barcode (not present in the warehouse)
            case this.OBJECT_TYPES.NEW:
                console.error(`Fix error: can not end moving object without a startMovingObject called previousely. barcode: ${barcode} forklift: ${forkliftId}`)
                break

            // 3. end moving a known object
            case this.OBJECT_TYPES.KNOWN:

                // set forklift status
                this._setForkliftStatus(forkliftId, this.ACTIONS.RELEASING_AN_OBJECT)

                // try to set new object status
                let object = this.warehouseByCodes.get()[barcode]
                objectIsUpdated = object.updatePositionAndStatus(position, warehouseObject.STATUS.STATIC)

                // if object is correctly updated
                if (objectIsUpdated) {
                    // update Freezer data (this triggers object data save)
                    this.warehouseByCodes.get().set(barcode, object)
                    this.warehouseByCodes.get().set('lastObjectCode', barcode)
                    // add to object history the new position and action
                    this._addHistoryEntry(barcode, position, forkliftId, this.ACTIONS.MOVING_AN_OBJECT)
                } else {
                    this.errorMessage = object.errorMessage
                }

                break
        }

        return objectIsUpdated
    }

    _identifyObjectTypeByBarcode(barcode) {

        // identify movementation situation
        let objectType
        if (typeof barcode == this.UNDEFINED) objectType = this.OBJECT_TYPES.UNKNOWN
        else if (this.getObjectByBarcode(barcode) == this.UNDEFINED) objectType = this.OBJECT_TYPES.NEW
        else objectType = this.OBJECT_TYPES.KNOWN

        return objectType
    }

    _objectExistInWarehouse(code) {
        return (this.getObjectByBarcode(code) != this.UNDEFINED)
    }

    guessObjectFromPosition(position) {

    }

    getObjectByBarcode(barcode) {
        if (!typeof barcode == "string") { throw new Error("Parameter type error: string expected") }

        // return undefined if barcode does not exist
        if (typeof this.warehouseByCodes.get()[barcode] == "undefined") { return this.UNDEFINED }
        // return the object found
        return this.warehouseByCodes.get()[barcode]
    }

    getObjectsByType(type) {
        // what types are?
    }


    //
    // PRIVATE METHODS
    //


    /**
     * Log to console or pass arguments to an external function {@link #configurationObject}
     *
     * @param {...string} text  to log
     * @memberof wareHouseClass
     */
    _logAction(...strings) {
        // pass all params to logger function
        if (_isFunction(this.externalLogger)) {
            this.externalLogger.apply(null, strings)
        }
        // console log
        if (this.conf.enableConsoleLog) {
            console.log.apply(null, strings)
        }
    }

    _setObjectDataByBarcode(code, pos, stat) {

        // if the object is not in the warehouse return the error
        if (!this._objectExistInWarehouse(code)) return false

        // get object to update
        let objectToUpdate = this.warehouseByCodes.get()[code].toJS()

        // set updated object to warehouse
        objectToUpdate = this._setObjectData(objectToUpdate, pos, stat)
        if (objectToUpdate === false) {
            return false
        }

        this.warehouseByCodes.get()
                             .set(code, objectToUpdate)
                             .set('lastObjectCode', code)

        return true
    }

    _setObjectData(obj, pos, stat) {

        // if position and status are not defined, change nothing
        if (typeof pos != this.UNDEFINED || typeof stat != this.UNDEFINED) {

            // set new position
            if (typeof pos != this.UNDEFINED) {
                // position parameter check
                if (typeof pos != "object") {
                    throw "Parameter type error: object expected"
                }
                if (typeof pos.x != "number" || typeof pos.y != "number" || typeof pos.z != "number") {
                    throw "Parameter type error: at least property has wrong type"
                }
                // passed object-to-set check
                if (typeof obj.position_x == this.UNDEFINED || typeof obj.position_y == this.UNDEFINED || typeof obj.position_z == this.UNDEFINED) {
                    throw "Parameter type error: passed object to set has not, at lease, one good position property"
                }
                // set new position data
                obj.position_x = pos.x
                obj.position_y = pos.y
                obj.position_z = pos.z
            }

            // set status
            if (typeof stat != "undefined") {
                // status parameter check
                if (typeof stat != "string") {
                    throw "Parameter type error: string expectede"
                }
                if (Object.values(this.OBJECT_STATUSES).indexOf(stat) == -1) {
                    throw "Parameter value error: value not allowed"
                }
                // passed object-to-set check
                if (typeof obj.lastStatus == this.UNDEFINED) {
                    throw "Parameter type error: passed object to set has not lastStatus property"
                }
                // set new status data
                obj.lastStatus = stat
            }

            // passed object-to-set check
            if (typeof obj.lastActionDateTime_ts == this.UNDEFINED) {
                throw "Parameter type error: passed object to set has not lastActionDateTime_ts property"
            }

            // update last datetime change
            obj.lastActionDateTime_ts = + new Date()
            obj.lastActionDateTime_hr = this._readableItalianDateTime
        }

        return obj
    }

    _queryObjectInfo(barcode) {
        // add code to remotely ask data informations
        // such as: width, height, weight, object type (paper type), ...
    }

    _askObjectData(object, type = undefined) {
        if ( type === undefined ) type = lib_shmMathLib.getRandomIntInclusive(0, 2)     // TODO: REMOVE
        return this.__REMOVEME__generateFakeObjectData(type)                            // TODO: REMOVE
    }

    _addHistoryEntry(code, pos, forId, act) {
        let timestampNow = + new Date()

        // create object and assign values
        let objHistoryAction = {
            code: code,
            datetime_ts: timestampNow,
            datetime_readable: this._readableItalianDateTime,
            forklift_id: forId,
            position_x: pos.x,                  // distance in centimeters along x axis from object base center to warehouse origin
            position_y: pos.y,                  // distance in centimeters along y axis from object base center to warehouse origin
            position_z: pos.z,                  // distance in centimeters along z axis from object base center to the ground
            action: act                         // action string, one of this.ACTIONS
        }

        // keep history in memory
        if (this.conf.warehouseByCodesHistoryInMemory) {
            if (typeof this.warehouseByCodesHistory[code] == this.UNDEFINED) {
                // create object history
                this.warehouseByCodesHistory[code] = []
            }
            // push history object
            this.warehouseByCodesHistory[code].push(objHistoryAction)
        }

        // store data in database
        this._storeData("action", "add", objHistoryAction)
    }

    _setForkliftStatus(id, stat) {
        // parameters check
        if (arguments.length != 2) { throw new Error("Parameters error: expected 2 arguments") }
        if (!Number.isInteger(id)) { throw new TypeError("Parameter type error: integer expected") }
        if (typeof stat != "string") { throw new TypeError("Parameter type error: string expected") }
        if (Object.values(this.ACTIONS).indexOf(stat) == -1) { throw new Error("Parameter type error: expect one of this.ACTIONS constants") }

        // check new status against older one
        this._checkForkliftActionCoherence(id, stat)

        // set forklift new status
        this.forkLiftsWork[id] = stat

        // log status
        this._logAction(`FORK ${id} => ${stat.toUpperCase()}`)

        return true
    }

    /**
     * Set forklift status.
     * The new status is checked against the previous one: if not coherent
     * a log entry is added
     *
     * @method _checkForkliftActionCoherence
     * @param {number} id forklift identification number
     * @param {string} stat new status (one of this.ACTIONS constants)
     * @returns
     * @memberof wareHouseClass
     */
    _checkForkliftActionCoherence(id, stat) {

        // if current forkliftWork is undefined, the class was just instanced
        if (typeof this.forkLiftsWork[id] == this.UNDEFINED) {
            this._logAction(`System just started and/or forklift status is unknown: the first status is always accepted`)
            return true
        }

        let goodStats
        let acts = this.ACTIONS
        let actsReleasing = [acts.RELEASING_AN_OBJECT, acts.RELEASING_AN_UNKNOWN_OBJECT, acts.RELEASING_A_NEW_OBJECT]
        let actsMoving = [acts.MOVING_AN_OBJECT, acts.MOVING_AN_UNKNOWN_OBJECT, acts.MOVING_A_NEW_OBJECT]

        // local log function
        let closeThis = this
        let consoleLogErr = function (sts, ps, cs, fid) {
            let goodStats = sts.map(s => s.toUpperCase())
            closeThis._logAction(`FORK ${fid}: setting to ${ps.toUpperCase()} but previousely was ${cs.toUpperCase()}`)
            if (goodStats.length == 1) {
                closeThis._logAction(`      : only ${goodStats.toString()} accepted`)
            } else {
                closeThis._logAction(`        only one of ${goodStats.toString()} accepted`)
            }
        }

        let curStat = this.forkLiftsWork[id]
        switch (stat) {
            // switching forklift on
            // previous status shoud be a FORKLIFT_SWITCHED_OFF
            case this.ACTIONS.FORKLIFT_SWITCHED_ON:
                // good previous statuses
                goodStats = [acts.FORKLIFT_SWITCHED_OFF]
                if (goodStats.indexOf(curStat) == -1) {
                    consoleLogErr(goodStats, stat, curStat, id)
                }
                break
            // switching forklift off
            // previous statuses shoud be one of RELEASING_
            case this.ACTIONS.FORKLIFT_SWITCHED_OFF:
                // good previous statuses
                goodStats = actsReleasing
                goodStats.push(this.ACTIONS.FORKLIFT_SWITCHED_ON)
                // now check
                if (goodStats.indexOf(curStat) == -1) {
                    consoleLogErr(goodStats, stat, curStat, id)
                }
                break
            // forklift releasing object
            // previous statuses shoud be one of MOVING_
            case this.ACTIONS.RELEASING_AN_OBJECT:
            case this.ACTIONS.RELEASING_AN_UNKNOWN_OBJECT:
            case this.ACTIONS.RELEASING_A_NEW_OBJECT:
                // good previous statuses
                goodStats = actsMoving
                if (goodStats.indexOf(curStat) == -1) {
                    consoleLogErr(goodStats, stat, curStat, id)
                }
                break
            // forklift moving object
            // previous statuses shoud be one of RELEASING_ or SWITCHED_ON
            case this.ACTIONS.MOVING_AN_OBJECT:
            case this.ACTIONS.MOVING_AN_UNKNOWN_OBJECT:
            case this.ACTIONS.MOVING_A_NEW_OBJECT:
                // good previous statuses
                goodStats = actsReleasing
                goodStats.push(this.ACTIONS.FORKLIFT_SWITCHED_ON)
                if (goodStats.indexOf(curStat) == -1) {
                    consoleLogErr(goodStats, stat, curStat, id)
                }
                break
        }
    }

    _storeData(what, action, data) {
        // select enabled storage
        // SQLite
        if (this.conf.store.enabled.indexOf(this.SUPPORTED_STORAGE_SYSTEM.SQLite) != -1) {
            // store data according action
            switch (what) {
                case "object":
                    switch (action) {
                        // object -> add
                        case "add":
                            this.STORE_SQLite.addNewObject(data)
                            break
                        // object -> update
                        case "update":
                            this.STORE_SQLite.updateObjectData(data)
                            break
                    }
                break
                case "action":
                    switch (action) {
                        case "add":
                            this.STORE_SQLite.addNewHistoryEntry(data)
                            break
                    }
            }
        }
    }

    __REMOVEME__generateFakeObjectData(type) {                 // REMOVE
        let obj = {}
        switch (type) {
            // cylinder: 148 (diameter) x 130 (height), 1.45 tons
            case 0:
                obj.length = 148
                obj.width = 148
                obj.height = 130
                obj.weight = 1450
                obj.materialType = "carta patinata"
                obj.materialSubType = "gr. 200"
                break
            case 1:
                obj.length = 125
                obj.width = 125
                obj.height = 100
                obj.weight = 1100
                obj.materialType = "carta glossy"
                obj.materialSubType = "gr. 250"
                break
            case 2:
                obj.length = 45
                obj.width = 45
                obj.height = 80
                obj.weight = 650
                obj.materialType = "carta speciale"
                obj.materialSubType = "gr. 180"
                break
        }

        this._logAction(`Generated a cylinder ${obj.length}x${obj.width}x${obj.height}`)

        return obj
    }
}


var warehouse = new wareHouseClass()

module.exports = warehouse

class warehouseObject {

    constructor(barcode = undefined) {

        // check barcode parameter
        if (typeof barcode != "string" && typeof barcode != "undefined" ) { throw new TypeError("Parameter type error: string expected") }

        let tsNow = + new Date()                        // timestamp
        let tsHrNow = this._readableItalianDateTime     // timestamp human readable

        // declare properties and assign barcode and creation datetime
        this.code = barcode                             // can be string or undefined
        this.created_ts = tsNow
        this.lastActionDT = {
            timestamp: tsNow,
            humanReadable: tsHrNow
        }
        this.dimensions = {                             // in centimeters
            length: undefined,
            width: undefined,
            height: undefined
        }
        this.weight = undefined                         // in hundreds of kilograms
        this.material = {
            type: undefined,
            subType: undefined
        }
        this.position = {                               // in centimeters
            x: undefined,                                 // distance along x axis from object base center to warehouse origin
            y: undefined,                                 // distance along y axis from object base center to warehouse origin
            z: undefined                                  // distance along z axis from object base center to the ground
        }
        this.status = undefined                          // one of this.OBJECT_STATUSES

        this.errorMessage = ''
    }

    static get STATUS() {
        return {
            STATIC: "static",
            MOVING: "moving",
            PROCESSING: "processing",
            PROCESED: "processed"
        }
    }

    setDataFromSQLRecordToObject(record) {
        // set physical properties
        this.setPhysicalProperties({
                length: record.length_in_cm,
                width: record.width_in_cm,
                height: record.height_in_cm
            },
            record.weight_in_hkg,
            {
                type: record.material_type,
                subType: record.$material_subtype
            }
        )
        // set position and status
        this.setPositionAndStatus({
                x: record.position_x,
                y: record.position_y,
                z: record.position_z
            },
            record.status
        )
    }

    setPhysicalProperties(dim, w, mat) {
        this._setDimensions(dim, w)
        this._setMaterial(mat)
    }

    /**
     * Assign position and status data
     * To perform coherence call updatePositionAndStatus method
     *
     * @method setPositionAndStatus
     * @param {*} pos
     * @param {*} st
     * @memberof warehouseObject
     */
    setPositionAndStatus(pos, st) {
        this._setPosition(pos)
        this._setStatus(st)
        this._updateLastActionDT()
    }

    /**
     * After checks, update position and status data
     * In order to assign directly data without checks call setPositionAndStatus method
     *
     * @method updatePositionAndStatus
     * @param {*} pos
     * @param {*} st
     * @memberof warehouseObject
     */
    updatePositionAndStatus(pos, newSt) {

        // PLEASE ADD parameters check

        // allowed statuses
        let allowedStatuses = {
            "static": [warehouseObject.STATUS.MOVING],
            "moving": [warehouseObject.STATUS.STATIC, warehouseObject.STATUS.PROCESSING]
        }

        // check previous status before assign
        if ( allowedStatuses[newSt].indexOf(this.status) === -1 ) {
            let stStr = this.status.toUpperCase()
            let allstaStr = allowedStatuses[newSt].map(el => el.toUpperCase()).join(' or ')
            let errMsg = `Object must be ${allstaStr} but current status is ${stStr}`
            this.errorMessage = errMsg
            console.error(errMsg)
            return false
        }

        // log distance from previous known position
        let computedDistance = Math.floor(lib_shmMathLib.twoPointsDistance(this.position, pos))
        console.log(`Object ${this.code}: distance from actual position and the known one: ${computedDistance}cm`)

        this._setPosition(pos)
        this._setStatus(newSt)
        this._updateLastActionDT()

        return true
    }

    get getSQLiteInsertOrUpdateQueryAndData() {
        let tableName = "objects"
        let fields = ['code', 'code_type', 'status', 'length_in_cm', 'width_in_cm', 'height_in_cm',
                      'weight_in_hkg', 'material_type', 'material_subtype', 'position_x', 'position_y',
                      'position_z', 'creation_dt', 'last_action_hr', 'last_action_ts']

        let query = `INSERT OR REPLACE INTO ${tableName} (` + fields.join(', ') + ` ) VALUES (` + fields.map(el => `$${el}`).join(', ') + `);`

        let data = {
            $code: this.code,
            $code_type: "Code 128",
            $status: this.status,
            $length_in_cm: this.dimensions.length,
            $width_in_cm: this.dimensions.width,
            $height_in_cm: this.dimensions.height,
            $weight_in_hkg: this.weight,
            $material_type: this.material.type,
            $material_subtype: this.material.subType,
            $position_x: this.position.x,
            $position_y: this.position.y,
            $position_z: this.position.z,
            $creation_dt: this.created_ts,
            $last_action_hr: this.lastActionDT.timestamp,
            $last_action_ts: this.lastActionDT.humanReadable
        }

        return {
            query: query,
            data: data
        }
    }

    _setPosition(pos) {
        this.position.x = pos.x
        this.position.y = pos.y
        this.position.z = pos.z
    }

    _setDimensions(dim, w) {
        this.dimensions.length = dim.length
        this.dimensions.width = dim.width
        this.dimensions.height = dim.height
        this.weight = w
    }

    _setMaterial(mat) {
        this.material.type = mat.type
        this.material.subType = mat.subType
    }

    _setStatus(st) {
        this.status = st
    }

    _updateLastActionDT() {
        let tsNow = + new Date()                        // timestamp
        let tsHrNow = this._readableItalianDateTime     // timestamp human readable
        this.lastActionDT = {
            timestamp: tsNow,
            humanReadable: tsHrNow
        }
    }
}
class wareHouseData_SQLite_Class {

    constructor(filename, filepath) {
        let mode = sqlite3.OPEN_READWRITE|sqlite3.OPEN_CREATE
        this.db  = new sqlite3.Database(`${filepath}/${filename}`, mode)      // open sqlite db readwrite
    }

    loadOjectsData(cb) {
        let query = "SELECT * FROM objects"
        this.db.all(query, cb)
    }

    clearEveryData() {
        // delete queries
        //let query = []
        //query.push("DELETE FROM objects;")
        //query.push("DELETE FROM objects_positions_history;")
    }

    addNewObject(object) {
        this.updateObjectData(object)
    }

    updateObjectData(object) {
        let queryAndDAta = object.getSQLiteInsertOrUpdateQueryAndData
        this.db.run(queryAndDAta.query, queryAndDAta.data)
    }

    addNewHistoryEntry(entry) {
        let tableName = "objects_positions_history"
        let fields = ['object_code', 'movement_hr', 'movement_ts', 'action', 'forklift_id', 'position_x', 'position_y', 'position_z']

        let query = `INSERT INTO ${tableName} (` + fields.join(', ') + ` ) VALUES (` + fields.map(el => `$${el}`).join(', ') + `);`

        let data = {
            $object_code: entry.code,
            $movement_hr: entry.datetime_readable,
            $movement_ts: entry.datetime_ts,
            $action: entry.action,
            $forklift_id: entry.forklift_id,
            $position_x: entry.position_x,
            $position_y: entry.position_y,
            $position_z: entry.position_z
        }

        this.db.run(query, data)
    }
}