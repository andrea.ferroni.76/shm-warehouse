warehouseObject class
=====================

Methods:
  * public:
    * constructor(barcode = undefined)
    * setDataFromSQLRecordToObject(record)
    * setPhysicalProperties(dim, w, mat)
    * `setPositionAndStatus(pos, st)`: set new position and status and update operation datetime. Unlike `updatePositionAndStatus(pos, st)` this method does not perform status coherences checks
    * `updatePositionAndStatus(pos, newSt)`
  * private:
    * _setPosition(pos)
    * _setDimensions(dim, w)
    * _setMaterial(mat)
    * _setStatus(st)
    * _updateLastActionDT()

Getters:
  * `getSQLiteInsertOrUpdateQueryAndData`

Public properties:
  * `code`: a string, generally a barcode
  * `created_ts`: creation timestamp
  * `lastActionDT`: last action date and time
    * `timestamp`
    * `humanReadable`
  * `dimension`: sizes
    * `length`
    * `width`
    * `height`
  * `weight`
  * `material`
    * `type`
    * `subtype`
  * `position`:
    * `x`
    * `y`
    * `z`
  * `status`
  * `errorMessage`

Details
-------

### Constructor

When instancing new object a string parameter (the barcode) must be passed.
All properties are set to `undefined` except `created_ts`, `lastActionDT` fields and `code`


### setDataFromSQLRecordToObject

Assign properties data from SQL data record (see format)


### setPhysicalProperties

Assign dimensions, weight and and material properties


### setPositionAndStatus

Assign position and status properties and update last action date and time properties too


### updatePositionAndStatus

Works like setPositionAndStatus but some checks are performed.

Currently implemented checks are:

1. new status can change only if current one is allowed:
  * an object can be set STATIC only if actual status is MOVING
  * an object can be set MOVING only if actual status is STATIC or PROCESSING
  * ...more to come
2. more to come?


### getSQLiteInsertOrUpdateQueryAndData

Return `INSERT OR UPDATE` query and data according `sqlite3` library format

----

Private methods are not documented because very straightforward implementation