let appConfiguration = {
    "moduleName": "Warehouse",
    "version": "0.4.2",
    "webServer": {
        "port": 1050
    },
    "database": {
        "SQLite": {
            "filename": "warehouse.sqlite",
            "filepath": "./data"
        }
    }
}

module.exports = appConfiguration